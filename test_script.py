
from TemplateFitter import *

SM_file = ROOT.TFile("/afs/cern.ch/work/p/pverschu/private/TopXSec/CombineHists/SMEFT_SM_pttt.root");
cqq1_2_file = ROOT.TFile("/afs/cern.ch/work/p/pverschu/private/TopXSec/CombineHists/SMEFT_cQQ1_253_pttt.root");
cqq1_5_file = ROOT.TFile("/afs/cern.ch/work/p/pverschu/private/TopXSec/CombineHists/SMEFT_cQQ1_501_pttt.root");
cqq1_m2_file = ROOT.TFile("/afs/cern.ch/work/p/pverschu/private/TopXSec/CombineHists/SMEFT_cQQ1_m253_pttt.root");
cqq1_m5_file = ROOT.TFile("/afs/cern.ch/work/p/pverschu/private/TopXSec/CombineHists/SMEFT_cQQ1_m501_pttt.root");

var_name = "truth"

hist_SM = SM_file.Get(var_name)
hist_2 = cqq1_2_file.Get(var_name)
hist_5 = cqq1_5_file.Get(var_name)
hist_m2 = cqq1_m2_file.Get(var_name)
hist_m5 = cqq1_m5_file.Get(var_name)

parameter_list = ['cqq1']
func_form = 'pol'

temp = TemplateFitter(parameter_list, func_form)

temp.add_template([0.0], hist_SM)
temp.add_template([25.3], hist_2)
temp.add_template([50.1], hist_5)
temp.add_template([-25.3], hist_m2)
temp.add_template([-50.1], hist_m5)

temp.fit()

temp.plot_functions()

temp.plot_templates()
